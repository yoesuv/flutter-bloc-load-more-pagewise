import 'package:bloc_loadmore/src/bloc/pagination_bloc.dart';
import 'package:bloc_loadmore/src/data/constants.dart';
import 'package:bloc_loadmore/src/models/post_response.dart';
import 'package:bloc_loadmore/src/widgets/item_post.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';

class Pagination extends StatelessWidget {

  static const String routeName = 'pagination';

  @override
  Widget build(BuildContext context) {
    final PaginationBloc _bloc = PaginationBloc();
    return Scaffold(
      appBar: AppBar(
        title: Text('Pagination', style: TextStyle(fontWeight: FontWeight.bold)),
      ),
      body: PagewiseListView<Post>(
        padding: const EdgeInsets.symmetric(vertical: 8),
        pageSize: POST_LIMIT,
        itemBuilder: _itemBuilder,
        pageFuture: (int index)  => _bloc.getListPost(index * POST_LIMIT),
      )
    );
  }

  Widget _itemBuilder(BuildContext context, Post post, int index) {
    return ItemPost(post);
  }

}