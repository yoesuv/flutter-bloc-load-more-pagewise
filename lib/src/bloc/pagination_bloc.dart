import 'package:bloc_loadmore/src/models/post_response.dart';
import 'package:bloc_loadmore/src/repositories/app_repository.dart';

class PaginationBloc {

  final AppRepository _appRepository = AppRepository();

  Future<List<Post>> getListPost(int start) async {
    try {
      return await _appRepository.getListPost(start);
    } catch (err) {
      rethrow;
    }
  }

}