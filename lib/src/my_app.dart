import 'package:bloc_loadmore/src/routes/app_route.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.teal
      ),
      onGenerateRoute: AppRoute.routes,
    );
  }

}