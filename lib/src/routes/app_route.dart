import 'package:bloc_loadmore/src/screens/home.dart';
import 'package:bloc_loadmore/src/screens/pagination.dart';
import 'package:flutter/material.dart';

class AppRoute {

  static Route<dynamic> routes(RouteSettings settings) {
    if (settings.name == '/') {
      return MaterialPageRoute<dynamic>(
        builder: (BuildContext context) {
          return Home();
        }
      );
    } else if (settings.name == Pagination.routeName) {
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) {
            return Pagination();
          }
      );
    } else {
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) {
            return const Scaffold(
              body: Center(
                child: Text('Page Not Found'),
              ),
            );
          }
      );
    }
  }

}