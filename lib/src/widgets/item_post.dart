import 'package:bloc_loadmore/src/models/post_response.dart';
import 'package:flutter/material.dart';

class ItemPost extends StatelessWidget {

  const ItemPost(this._post);

  final Post _post;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('${_post.id}. ${_post.title}', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
          Text(_post.body),
          const Divider(thickness: 1)
        ],
      ),
    );
  }

}