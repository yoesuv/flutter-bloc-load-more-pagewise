## Flutter Bloc Load More
#### Version 1.0.1

Research Pagination in Flutter. Download [apk file](https://www.dropbox.com/s/ck99ucfqgx2m8hf)

#### References
- [Medium](https://medium.com/saugo360/flutter-creating-a-listview-that-loads-one-page-at-a-time-c5c91b6fabd3)
- [Github](https://github.com/AbdulRahmanAlHamali/flutter_pagewise)

#### Screenshot
| Home | Pagination | Pagination | 
| ---- | ---- | ---- |
| ![img](https://i.imgur.com/FYBIRkp.jpg) | ![img](https://i.imgur.com/0h4Fhwz.jpg) | ![img](https://i.imgur.com/UeqC3TL.jpg) |

#### List Library
- [Dio](https://pub.dev/packages/dio)
- [Equatable](https://pub.dev/packages/equatable)
- [Flutter Pagewise](https://pub.dev/packages/flutter_pagewise)
